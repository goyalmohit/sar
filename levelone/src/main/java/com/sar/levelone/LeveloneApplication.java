package com.sar.levelone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeveloneApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeveloneApplication.class, args);
	}
}
