package com.sar.levelone.service;

import com.sar.levelone.Model.Car;
import com.sar.levelone.Model.CarFactory;
import com.sar.levelone.exception.ValidationException;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class LevelService {


    public String stockPrice(String stock, String currencyPreference) {

        if (stock.length() != 4) {
            throw new ValidationException("Stock Exchange Code must be four character long.");
        }
        Double stockPriceInINR = Math.random() * 1000, inrToDollor = 0.0155;
        String stockPrice, defaultPrice = roundOf(inrToDollor * stockPriceInINR) + " USD";

        if (currencyPreference == null) {
            stockPrice = defaultPrice;
        } else {
            switch (currencyPreference) {
                default:
                case "USD":
                    stockPrice = defaultPrice;
                    break;
                case "INR":
                case "inr":
                    stockPrice = roundOf(stockPriceInINR) + " INR";
                    break;
                case "JPY":
                case "jpy":
                    stockPrice = roundOf(stockPriceInINR * 1.73) + " JPY";
                    break;
                case "EUR":
                case "eur":
                    stockPrice = roundOf(stockPriceInINR * 0.014) + " EUR";
                    break;
            }
        }

        return stockPrice;
    }

    // Using this method to get values till two decimal places.
    private double roundOf(double value) {
        return Math.round(value * 100.0) / 100.0;
    }

    public HashMap<String, String> getCarFeatures(String modelName) {
        CarFactory carFactory = new CarFactory();
        Car car = carFactory.getCar(modelName);
        if (car == null) {
            throw new ValidationException("Invalid Model Name: " + modelName);
        }
        return car.features();
    }
}
