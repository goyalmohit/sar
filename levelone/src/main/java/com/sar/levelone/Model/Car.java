package com.sar.levelone.Model;

import java.util.HashMap;

public interface Car {
    HashMap<String, String> features();
}
