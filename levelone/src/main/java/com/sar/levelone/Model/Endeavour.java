package com.sar.levelone.Model;

import java.util.HashMap;

public class Endeavour implements Car {

    @Override
    public HashMap<String, String> features() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Model", "Ford Endeavour");
        map.put("Type", "SUV");
        map.put("Price", "Rs 27.36 Lakh ");
        map.put("Average", "11 Kmpl ");
        map.put("Clearance", "225 mm");
        return map;
    }
}
