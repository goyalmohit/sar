package com.sar.levelone.Model;

import java.util.HashMap;

public class Mondeo implements Car {

    @Override
    public HashMap<String, String> features() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Model", "Ford Mondeo");
        map.put("Type", "Sedan");
        map.put("Price", "Rs 15 Lakh ");
        map.put("Average", "13 Kmpl ");
        map.put("Clearance", "112 mm");
        return map;
    }
}
