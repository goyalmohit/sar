package com.sar.levelone.Model;

import java.util.HashMap;
import java.util.Map;

public class Figo implements Car {

    @Override
    public HashMap<String, String> features() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Model", "Ford Figo");
        map.put("Type", "Hatchback");
        map.put("Price", "Rs 7.22 Lakh ");
        map.put("Average", "25.83 Kmpl ");
        map.put("Clearance", "170 mm");
        return map;
    }
}
