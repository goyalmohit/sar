package com.sar.levelone.Model;

import java.util.HashMap;

public class Ecosport implements Car {

    @Override
    public HashMap<String, String> features() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Model", "Ford Ecosport");
        map.put("Type", "Mini SUV");
        map.put("Price", "Rs 9.16 Lakh ");
        map.put("Average", "16 Kmpl ");
        map.put("Clearance", "200 mm");
        return map;
    }
}
