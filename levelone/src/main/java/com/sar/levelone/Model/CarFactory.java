package com.sar.levelone.Model;

public class CarFactory {

    public Car getCar(String modelName) {
        if (modelName == null) {
            return null;
        }
        if (modelName.equalsIgnoreCase("Figo")) {
            return new Figo();
        }
        if (modelName.equalsIgnoreCase("Ecosport")) {
            return new Ecosport();
        }
        if (modelName.equalsIgnoreCase("Mondeo")) {
            return new Mondeo();
        }
        if (modelName.equalsIgnoreCase("Endeavour")) {
            return new Endeavour();
        }
        return null;
    }
}
