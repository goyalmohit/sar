package com.sar.levelone.controller;

import com.sar.levelone.service.LevelService;
import com.sar.levelone.util.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/api/level/")
public class LevelController {

    @Autowired
    private LevelService levelService;

    @RequestMapping(value = "/one/{stock}/price", method = RequestMethod.GET)
    public JsonResponse stockPrice(@PathVariable String stock, @RequestParam(required = false) String currency) {
        return new JsonResponse(levelService.stockPrice(stock, currency));
    }

    @RequestMapping(value = "/two/{model}/features", method = RequestMethod.GET)
    public HashMap<String, String> carFeatures(@PathVariable String model) {
        return levelService.getCarFeatures(model);
    }
}
