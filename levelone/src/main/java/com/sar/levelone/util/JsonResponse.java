package com.sar.levelone.util;

public class JsonResponse {

    private String result;

    public JsonResponse(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
